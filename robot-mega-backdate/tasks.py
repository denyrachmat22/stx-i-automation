from robocorp.tasks import task
from robocorp import windows,log
from RPA.Database import Database
from RPA.Robocorp.Vault import FileSecrets
from urllib.request import urlopen

import json

desktop = windows.desktop()

@task
def automate_mega_backdate(app, urlBGData = ''):
    if urlBGData == '':
        listBG = FileSecrets("backdate.json")
    else:
        listBG = json.loads(urlopen(urlBGData).read().decode("utf-8"))

    desktop.windows_run(app + ".lnk")
    mega = windows.find_window('subname:"System Login"')
    try:
        mega.find('automationid:"txtUserID"').send_keys("ADMIN")
        mega.find('automationid:"txtPassword"').send_keys("$X1995@s")
        mega.find('automationid:"btnLoginOK"').click()
    finally:
        mega.close_window()
