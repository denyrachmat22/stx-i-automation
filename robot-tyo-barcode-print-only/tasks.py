import os
os.environ['HOMEPATH'] = '%LOCALAPPDATA%/robocorp'

from robocorp import browser
from robocorp.tasks import task
from RPA.Excel.Files import Files as Excel
import dateutil.parser as parser

from pathlib import Path
import glob

import ssl
import truststore
truststore.inject_into_ssl()

@task()
def printbyDate(frdate, todate, typePrint):
    browser.configure(
        browser_engine="firefox",
        screenshot="only-on-failure",
        headless=False,
    )
    
    browser.goto("https://epro5.b2b-exchange.jp/scm5/toyodenso/")
    
    try:
        login()
        onMainPage(frdate=frdate, todate=todate, typePrint=typePrint)
    finally:
        logout()

@task()
def printbyPO(po):
    browser.configure(
        browser_engine="firefox",
        screenshot="only-on-failure",
        headless=False,
    )
    
    browser.goto("https://epro5.b2b-exchange.jp/scm5/toyodenso/")
    
    try:
        login()
        onMainPage('', '', po)
    finally:
        logout()

def login():
    page = browser.page()

    page.fill(f"//input[@name='UID']", str('ID37005R'))
    page.fill(f"//input[@name='PWD']", str('6y51j7rd'))
    page.click("input:text('Login')")

    handle_dialog(page)

def onMainPage(frdate, todate, po = '', typePrint = ''):
    page = browser.page()

    for i in range(5):
        try:
            headerMenuPage = page.frame_locator('//*[@name="MENU"]')
            headerMenuPage.locator('//*[@id="menu_kaitou"]').hover(timeout=2000)
            headerMenuPage.locator('a', has_text= 'Shipment Data').click()

            break
        except:
            continue

    headerMainPage = page.frame_locator('//*[@name="MAIN"]')

    if (po != ''):
        headerMainPage.locator('//*[@name="ORDER_NO"]').fill(str(po))
    else :    
        if (typePrint == 'actual') :
            strType = 'ACTUAL'
        else:
            strType = 'REGIST'

        headerMainPage.locator('//*[@name="'+ strType +'_DATE_FROM_YEAR"]').select_option(str(parser.parse(frdate).year))
        headerMainPage.locator('//*[@name="'+ strType +'_DATE_FROM_MONTH"]').select_option(str(parser.parse(frdate).month))
        headerMainPage.locator('//*[@name="'+ strType +'_DATE_FROM_DAY"]').select_option(str(parser.parse(frdate).day))
        headerMainPage.locator('//*[@name="'+ strType +'_DATE_TO_YEAR"]').select_option(str(parser.parse(todate).year))
        headerMainPage.locator('//*[@name="'+ strType +'_DATE_TO_MONTH"]').select_option(str(parser.parse(todate).month))
        headerMainPage.locator('//*[@name="'+ strType +'_DATE_TO_DAY"]').select_option(str(parser.parse(todate).day))

    headerMainPage.get_by_role("button", name="Search").click(timeout=5000)
    page.wait_for_timeout(3000)
    table = headerMainPage.locator('#FRM > table:nth-child(3) > tbody')#FRM > table:nth-child(3) > tbody > tr.lnw
    if(table.count() > 0):
        headerMainPage.locator('#button2_1_1 > button:nth-child(4)').click() # Trigger download dulu
        
        files = glob.glob('D:\\website\\stx_api_v2\\storage\\app\\public\\upload_tyo_auto_bc_gen\\DownloadTYO\\DownloadedRangeDLVDate\\*')
        for f in files:
            os.remove(f)

        page.wait_for_timeout(3000)
        for d in range(2):
            onDownload('https://epro5.b2b-exchange.jp/scm5/home?pkg=ReplyData&fnc=List&act=printInvoice&PRINT_NUMBER=' + str(d + 1))
    else:
        raise Exception("Delivery date from " + frdate + " to " + todate + " is not found !!")

def handle_dialog(page):
    global statuser
    # page = browser.page()
    try:
        with page.expect_event("dialog", timeout=5000) as dialog_info:
            dialog = dialog_info.value

            # print(dialog.message)
            if "You can not create information of partial shipment" in dialog.message:
                statuser = {
                    'status': False,
                    'isDialog': True,
                    'msg' : "Sorry, PO No Already Exists"
                }
            elif "not found" in dialog.message:
                statuser = {
                    'status': False,
                    'isDialog': True,
                    'msg' : "Sorry, PO Not found"
                }
            elif "not existed" in dialog.message:
                statuser = {
                    'status': False,
                    'isDialog': True,
                    'msg' : "Sorry, PO already inputed, or not exists"
                }
            elif "Packing Style QTY" in dialog.message:
                statuser = {
                    'status': True,
                    'isDialog': True,
                    'msg' : "SPQ Less than qty"
                }
            elif "を押すと、現在ログイン中のユーザーが利用できなくなります" in dialog.message:
                statuser = {
                    'status': True,
                    'isDialog': True,
                    'msg' : "Another Login Existed"
                }
            elif "might trigger a repetition of any action you took there. Do you want to continue?" in dialog.message:
                statuser = {
                    'status': True,
                    'isDialog': True,
                    'msg' : "Confirm Resubmit"
                }
            elif "The length of" in dialog.message:
                statuser = {
                    'status': True,
                    'isDialog': True,
                    'msg' : "Confirm Submit Less Qty"
                }
            else:
                statuser = {
                    'status': True,
                    'isDialog': False,
                    'msg' : "Continue"
                }

            # print(statuser['msg'])
            if statuser['status'] == True and statuser['isDialog'] == True:
                dialog.accept()
            else:
                dialog.dismiss()
    except Exception as error:
        statuser = {
            'status': True,
            'msg' : error
        }

    # print(statuser['msg'])

    return statuser

def onDownload(urlToDownload):
    for i in range(5):
        try:
            page = browser.page()
            with page.expect_download(timeout= 5000) as download_info:
                try:
                    page.goto(urlToDownload, timeout=5000)
                except:
                    pass

            download = download_info.value

            download.save_as("D:\\website\\stx_api_v2\\storage\\app\\public\\upload_tyo_auto_bc_gen\\DownloadTYO\\DownloadedRangeDLVDate\\" + download.suggested_filename)

            break
        except:
            continue

def logout():
    try:
        page2 = browser.page()
        headerMenuPage2 = page2.frame_locator('/html/frameset/frame[1]')
        headerMenuPage2.locator('//*[@id="menu_logout"]').click(timeout=2000)
    except:
        page2 = browser.page()
        page2.close()