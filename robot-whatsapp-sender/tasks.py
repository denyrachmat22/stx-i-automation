from robocorp import browser
from robocorp.tasks import task
from RPA.Excel.Files import Files as Excel

from pathlib import Path
import os
import requests

@task
def waSender():
    """
    Solve the RPA challenge
    
    Downloads the source data excel and uses Playwright to solve rpachallenge.com from challenge
    """
    browser.configure(
        browser_engine="chromium",
        screenshot="only-on-failure",
        headless=False,
    )
    
    browser.goto("https://web.whatsapp.com/")
    try:
        page = browser.page()
        
        for i in range(5):
            try:
                getQRCode = page.locator('#app > div > div.landing-wrapper > div.landing-window > div.landing-main > div > div > div._ak96 > div').get_attribute('data-ref', timeout=5000)

                if not getQRCode:
                    print(getQRCode)

                    break
                else:
                    continue
            except:
                continue

    finally:
        # Place for teardown and cleanups
        # Playwright handles browser closing
        print('Done')

