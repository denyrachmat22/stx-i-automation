from robocorp.tasks import task
from robocorp import windows,log
from RPA.Database import Database
from RPA.Robocorp.Vault import FileSecrets
import json
import time
from urllib.request import urlopen
import datetime


desktop = windows.desktop()

filesecrets = FileSecrets("vault-mega-db.json")
secrets = filesecrets.get_secret("credentials")

def start_manual(): 
    if len(check_fifo_jobs_running()) > 0:
        log.console_message("Error: FIFO Customs still running waiting until done.", "Error")
    else:
        automate_closing(json.loads('{"app":"MEGA_EXIM"}'), "SME3XIZPHC") # type: ignore


def start_automation(data = ''):
    jsonFileLoad = json.loads(urlopen(data).read().decode("utf-8"))

    for jsonData in jsonFileLoad:
        automate_closing(jsonData["app"], jsonData["bgList"])

@task
def start_auto():
    if len(check_fifo_jobs_running()) > 0:
        log.console_message("Error: FIFO Customs still running waiting until done.", "Error")
    else :
        for dbs in secrets["vmi"]:
            db = Database()
            db.connect_to_database('pymssql',
                dbs["db"],
                secrets["username"],
                secrets["password"],
                '192.168.100.10'
            )
            listBG = db.query("SELECT * FROM MBSG_TBL")
            listAppendBG = []
            for bgs in listBG :
                listAppendBG.append(bgs["MBSG_BSGRP"])

                if len(check_current_closing_month_year(dbs['db'], bgs["MBSG_BSGRP"])) > 0:
                    err = f'Error: DB {dbs["db"]} & BG {bgs["MBSG_BSGRP"]} Closing month & year already same with current month.'
                else:
                    err = f'DB {dbs["db"]} & BG {bgs["MBSG_BSGRP"]} Continue closing'
                    automate_closing(dbs['app'], bgs["MBSG_BSGRP"]) # type: ignore
            
                print(err)

def check_fifo_jobs_running():
    db = Database()
    db.connect_to_database('pymssql',
        "VMI_DB",
        secrets["username"],
        secrets["password"],
        '192.168.100.10'
    )
    FIFOCheck = db.query("SELECT * FROM Z_STXI_CHECK_RUNNING_JOB WHERE job_name = 'CUSTOMS FIFO'")

    return FIFOCheck

def check_current_closing_month_year(dbSel, bg):
    db = Database()
    db.connect_to_database('pymssql',
        dbSel,
        secrets["username"],
        secrets["password"],
        '192.168.100.10'
    )

    current_date = datetime.datetime.now()

    # Use parameterized query with %s placeholders
    query = 'SELECT * FROM MCTL_TBL WHERE MCTL_CYEAR = %s and MCTL_CMTH = %s and MCTL_PRJCD = %s;'
    ClosingDateCheck = db.query(query, data=(current_date.year, current_date.month, bg))

    return ClosingDateCheck

def check_jobs_status(jobs):
    db = Database()
    db.connect_to_database('pymssql',
        'master',
        secrets["username"],
        secrets["password"],
        '192.168.100.10'
    )

    query = "SELECT name, enabled, * FROM msdb.dbo.sysjobs WHERE name = ?"
    jobsCheck = db.query(query, data=(jobs))

    return jobsCheck
    
def automate_closing(appmega, bg):
    print(bg)
    desktop.windows_run(appmega + ".lnk")
    mega = windows.find_window('subname:"System Login"', timeout=120)
    
    try:
        mega.find('automationid:"txtUserID"').send_keys("ADMIN")
        mega.find('automationid:"txtPassword"').send_keys("$X1995@s")
        mega.find('automationid:"btnLoginOK"').click()
        if appmega == 'MEGA_EXIM':
            megaMainMenu = windows.find_window('regex:.*MEGA EXIM')
        elif appmega == 'MEGA_TYO':
            megaMainMenu = windows.find_window('regex:.*MEGA TOYO DENSO')
        else:
            megaMainMenu = windows.find_window('regex:.*MEGA EMS')

        megaMainMenu.find('control:"MenuItemControl" and name:"File"').click()
        megaMainMenu.find('name:"Change Operating Business Group"').click()
        megaMainMenu.find('path:"3|1|3|1" and automationid:"btnFind"').click()
        megaMainMenu.find('automationid:"txtKey"').send_keys(bg)
        megaMainMenu.find('automationid:"btnSelect"').click()
        megaMainMenu.find('path:"3|1|5|2" and automationid:"btn_OK"').click()
        megaMainMenu.find('regex:.*sucessfully')
        megaMainMenu.find('name:"OK" and class:"Button" and path:"1|1" and automationid:"2"').click()
        megaMainMenu.find('name:"MEGA EMS Periodic" and handle:"0" and path:"3|1|1|2|1|15"').double_click()
        megaMainMenu.find('name:"Month End & Cost Calculation" and handle:"0" and path:"3|1|1|2|1|16|2"').double_click()
        closing_action()   
        # for bgs in bg:
                     
            # megaMenuClosing.find('regex:.*Monthly FIFO Costing Calculation Completed!', timeout=5)
            # megaMenuClosing.find('name:"OK"').click()
            # for i in range(3):
            #     try:
            #         megaMenuClosing = windows.find_window('regex:.*Calling from MEGA EMS', timeout=1800)
            #         megaMenuClosing.find('control:"CheckBoxControl" and class:"ThunderRT6CheckBox" and name:"Perform Month End Closing"').click()
            #         megaMenuClosing.find('name:"OK"').click()
            #         for j in range(3):
            #             try:
            #                 megaMenuClosing2 = windows.find_window('regex:.*Calling from MEGA EMS', timeout=1800)
            #                 megaMenuClosing2.find('regex:.*Monthly FIFO Costing Calculation Completed!', timeout=5)
            #                 megaMenuClosing2.find('name:"OK"').click()
            #             except:
            #                 break

            #         break
            #     except:
            #         continue
    finally:
        print(f"Done")

def closing_action(retry_err = 0):
    try:
        megaMenuClosing = windows.find_window('regex:.*Calling from MEGA EMS', timeout=1800)
        megaMenuClosing.find('control:"CheckBoxControl" and class:"ThunderRT6CheckBox" and name:"Perform Month End Closing"').click()
                # megaMenuClosing.close_window()
        megaMenuClosing.find('name:"OK"').click()
                
        megaMenuClosing.find_child_window('regex:.*Monthly FIFO Costing Calculation Completed!', timeout=60)
        megaMenuClosing.find('name:"OK"').click()
    except Exception as e:
        print(f"Error: {e} Retry No {retry_err}")
        print(f"Waiting 1 minute before trying again !!")
        time.sleep(50)
        if(retry_err < 150):
            closing_action(retry_err + 1)