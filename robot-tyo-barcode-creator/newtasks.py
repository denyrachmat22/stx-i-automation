import os
import unicodedata
os.environ['HOMEPATH'] = '%LOCALAPPDATA%/robocorp'
# os.environ["ROBOCORP_HOME"] = "%LOCALAPPDATA%/robocorp"

from robocorp import browser, log
from robocorp.tasks import task
from robocorp import vault
from urllib.request import urlopen
import json
import time

statuser = {
    'status': False,
    'msg' : ""
}

@task
def minimal_task():
    browser.configure(
        browser_engine="firefox",
        screenshot="only-on-failure",
        headless=False
    )

    try:
        browser.goto("https://epro5.b2b-exchange.jp/scm5/toyodenso/")

        # jsonFileLoad = json.loads(urlopen(data).read().decode("utf-8"))
        # login(jsonFileLoad)
        login()
    finally:
        print('Done')

def login():
    page = browser.page()

    page.locator("[name=UID]").fill("ID37005R")
    page.locator("[name=PWD]").fill("6y51j7rd")
    page.locator("#B_LOGIN").click()

    with page.expect_event("dialog", timeout=5000) as dialog_info:
        dialog = dialog_info.value

        print(dialog.message)