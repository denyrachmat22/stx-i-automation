
import os
import unicodedata
os.environ['HOMEPATH'] = '%LOCALAPPDATA%/robocorp'
# os.environ["ROBOCORP_HOME"] = "%LOCALAPPDATA%/robocorp"

from robocorp import browser, log
from robocorp.tasks import task
from robocorp import vault
from urllib.request import urlopen
import json
import time

statuser = {
    'status': False,
    'msg' : ""
}

@task
def minimal_task(data):
    browser.configure(
        browser_engine="firefox",
        screenshot="only-on-failure",
        headless=False
    )

    try:
        browser.goto("https://epro5.b2b-exchange.jp/scm5/toyodenso/")

        jsonFileLoad = json.loads(urlopen(data).read().decode("utf-8"))
        login(jsonFileLoad)
    finally:
        print('Done')

def onDialogAlreadyLogin(dialog) :
    # print(dialog.message)

    assert dialog.message == 'ID37005R'
    dialog.accept()

def login(data):
    page = browser.page()

    page.locator("[name=UID]").fill("ID37005R")
    page.locator("[name=PWD]").fill("6y51j7rd")
    page.locator("#B_LOGIN").click()

    handle_dialog(page)
    onMainPage(page, data)

def onMainPage(page, data):
    try:
        page = browser.page()

        for dataJSON in data :
            # page = browser.page()

            checkDataExists = reprintData(page, str(dataJSON['po_no']), dataJSON['qty'])

            if checkDataExists == True:
                pass
            else:        
                for i in range(5):
                    try:
                        headerMenuPage = page.frame_locator('//*[@name="MENU"]')
                        headerMenuPage.locator('#menu_nonyuhenkou').hover()
                        headerMenuPage.locator('a', has_text= 'Input with Keyboard').click()

                        break
                    except:
                        continue
                    
                headerMainPage = page.frame_locator('//*[@name="MAIN"]')
                # page.wait_for_timeout(3000)
                for i in range(5):
                    try:
                        headerMainPage.locator('//*[@name="ORDER_NO_INPUT"]').fill(str(dataJSON['po_no']), timeout=3000)
                        page.keyboard.down("Enter")

                        break
                    except:
                        continue

                # cek = page.on("dialog", lambda: handle_dialog2)
                handle_dialog(page)
                if statuser['status'] == True :
                    headerMainPage.locator('//*[@name="ACTUAL_DATE"]').click()
                    page.keyboard.press("Control+A")
                    page.keyboard.press("Backspace")

                    headerMainPage.locator('//*[@name="ACTUAL_QTY"]').click()
                    page.keyboard.press("Control+A")
                    page.keyboard.press("Backspace")

                    headerMainPage.locator('//*[@name="PRODUCTION_DATE_LOT"]').click()
                    page.keyboard.press("Control+A")
                    page.keyboard.press("Backspace")

                    headerMainPage.locator('//*[@name="VENDOR_LOT_NO"]').click()
                    page.keyboard.press("Control+A")
                    page.keyboard.press("Backspace")

                    headerMainPage.locator('//*[@name="PACKING_STYLE_QTY_CHANGE"]').click()
                    page.keyboard.press("Control+A")
                    page.keyboard.press("Backspace")
                    
                    headerMainPage.locator('//*[@name="ACTUAL_DATE"]').fill(dataJSON['date'])
                    headerMainPage.locator('//*[@name="ACTUAL_QTY"]').fill(str(dataJSON['qty']))
                    headerMainPage.locator('//*[@name="PRODUCTION_DATE_LOT"]').fill(dataJSON['job_no'])
                    headerMainPage.locator('//*[@name="VENDOR_LOT_NO"]').fill(dataJSON['job_no'])
                    headerMainPage.locator('//*[@name="PACKING_STYLE_QTY_CHANGE"]').fill(str(dataJSON['spq']))

                    for i in range(5):
                        try:
                            headerMainPage.get_by_role("button", name="OK").click(timeout=5000)
                            handle_dialog(page)
                            # page.wait_for_timeout(5000)

                            break
                        except:
                            continue

                    # time.sleep(5)

                    for i in range(3):
                        try:
                            headerMainPage.locator('//*[@id="PRINT"]').click(timeout=5000)

                            time.sleep(3)
                            # Start waiting for the download

                            for d in range(2):
                                onDownload('https://epro5.b2b-exchange.jp/scm5/home?pkg=ReplyData&fnc=List&act=printInvoice&PRINT_NUMBER=' + str(d + 1), dataJSON['po_no'])

                                # page.wait_for_timeout(5000)

                            break
                        except:
                            if i == 2:
                                raise Exception('Cannot find Print button, please resubmit again !!')
                            else:    
                                headerMainPage.get_by_role("button", name="OK").click(timeout=5000)
                                handle_dialog(page)
                                # page.wait_for_timeout(5000)
                            continue
                else:
                    raise Exception(statuser['msg'])
    finally:
        logout(page)

def onDownload(urlToDownload, po):
    for i in range(5):
        try:
            page = browser.page()
            with page.expect_download(timeout= 5000) as download_info:
                try:
                    page.goto(urlToDownload, timeout=5000)
                except:
                    pass

            download = download_info.value

            download.save_as("D:\\website\\stx_api_v2\\storage\\app\\public\\upload_tyo_auto_bc_gen\\DownloadTYO\\" + po + "\\" + download.suggested_filename)

            break
        except:
            continue

def handle_dialog(page):
    global statuser
    # page = browser.page()
    try:
        with page.expect_event("dialog", timeout=5000) as dialog_info:
            dialog = dialog_info.value

            # print(dialog.message)
            if "You can not create information of partial shipment" in dialog.message:
                statuser = {
                    'status': False,
                    'isDialog': True,
                    'msg' : "Sorry, PO No Already Exists"
                }
            elif "not found" in dialog.message:
                statuser = {
                    'status': False,
                    'isDialog': True,
                    'msg' : "Sorry, PO Not found"
                }
            elif "not existed" in dialog.message:
                statuser = {
                    'status': False,
                    'isDialog': True,
                    'msg' : "Sorry, PO already inputed, or not exists"
                }
            elif "Packing Style QTY" in dialog.message:
                statuser = {
                    'status': True,
                    'isDialog': True,
                    'msg' : "SPQ Less than qty"
                }
            elif "を押すと、現在ログイン中のユーザーが利用できなくなります" in dialog.message:
                statuser = {
                    'status': True,
                    'isDialog': True,
                    'msg' : "Another Login Existed"
                }
            elif "might trigger a repetition of any action you took there. Do you want to continue?" in dialog.message:
                statuser = {
                    'status': True,
                    'isDialog': True,
                    'msg' : "Confirm Resubmit"
                }
            elif "The length of" in dialog.message:
                statuser = {
                    'status': True,
                    'isDialog': True,
                    'msg' : "Confirm Submit Less Qty"
                }
            else:
                statuser = {
                    'status': True,
                    'isDialog': False,
                    'msg' : "Continue"
                }

            # print(statuser['msg'])
            if statuser['status'] == True and statuser['isDialog'] == True:
                dialog.accept()
            else:
                dialog.dismiss()
    except Exception as error:
        statuser = {
            'status': True,
            'msg' : error
        }

    # print(statuser['msg'])

    return statuser

def reprintData(page, orderNo, willSubmitedQty):
    # page = browser.page()
    headerMenuPage = page.frame_locator('//*[@name="MENU"]')
    headerMenuPage.locator('//*[@id="menu_kaitou"]').hover()
    headerMenuPage.locator('a', has_text= 'Shipment Data').click()

    headerMainPage = page.frame_locator('//*[@name="MAIN"]')
    headerMainPage.locator('//*[@name="ORDER_NO"]').fill(orderNo)

    headerMainPage.get_by_role("button", name="Search").click(timeout=5000)
    page.wait_for_timeout(3000)
    
    table = page.frame_locator('//*[@name="MAIN"]').locator('form#FRM > table:nth-child(3) > tbody > tr')#FRM > table:nth-child(3) > tbody > tr.lnw
    totalTable = table.count()

    # IF PO Not Inputed yet
    if(totalTable > 1):
        page.wait_for_timeout(2000)

        # Get Submited Data
        parseTot = 0
        submitedTotal = 0
        for iTbl in range(totalTable):            
            if iTbl > 0 :
                getQtyPOTotal = headerMainPage.locator('//*[@id="FRM"]/table[3]/tbody/tr[' + str(iTbl + 1) + ']/td[20]').inner_text(timeout=3000)                
                getQtyPOTotal = unicodedata.normalize("NFKD", getQtyPOTotal)

                getQtyDlvTotal = headerMainPage.locator('//*[@id="FRM"]/table[3]/tbody/tr[' + str(iTbl + 1) + ']/td[21]').inner_text(timeout=3000)                
                getQtyDlvTotal = unicodedata.normalize("NFKD", getQtyDlvTotal)

                parseTot = int(getQtyPOTotal.replace(',', ''))
                submitedTotal += int(getQtyDlvTotal.replace(',', ''))
                
        log.info([parseTot, submitedTotal])

        # IF Total PO Already full
        if(parseTot < (submitedTotal + int(willSubmitedQty))):
            headerMainPage.locator('//*[@id="button1_1_1"]/button[4]').click(timeout=5000)

            page.wait_for_timeout(2000)
        
            for d in range(2):
                onDownload('https://epro5.b2b-exchange.jp/scm5/home?pkg=ReplyData&fnc=List&act=printInvoice&PRINT_NUMBER=' + str(d + 1), orderNo)
                if d == 0:
                    page.wait_for_timeout(5000)
                    continue
                else:
                    break
            return True
        else:
            return False
    else:
        return False
        
def logout(page):
    try:
        headerMenuPage2 = page.frame_locator('/html/frameset/frame[1]')
        headerMenuPage2.locator('//*[@id="menu_logout"]').click(timeout=2000)
    except:
        page.close()